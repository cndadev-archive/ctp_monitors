#!/bin/bash

# SCRIPT PREREQUISITE
#  DCMTK must be installed and either in the path or the DCMTKDIR must be exported

EMAILTXTDIR=${TOOLPATH}/emailTXT
EMAILBODYFILE=${TOOLPATH}/.tmpEmailBodyFile.txt
MUTTRCFILE=${TOOLPATH}/checks.muttrc

if [ -s $EMAILBODYFILE ]; then
   rm $EMAILBODYFILE
fi
touch $EMAILBODYFILE

if [ $# -lt 5 ]; then
   echo "Usage:  ./dicomPing.sh aeTitle host port fileStoragePath emailList"
   exit 1
fi

aeTitle=$1
host=$2
port=$3
fileStoragePath=$4
emails=$5

if [ ! -z ${DCMTKDIR} ]; then
    ${DCMTKDIR}/dcmsend -aec ${aeTitle} ${host} ${port} ${TOOLPATH}/test_dicom/*.dcm
else
    dcmsend -aec ${aeTitle} ${host} ${port} ${TOOLPATH}/test_dicom/*.dcm
fi

# Wait to make sure file has time to be stored by CTP
sleep 120

# Check to see if file made it to storage
fileExists=`find ${fileStoragePath} -name "*.dcm"`
if [ -z "$fileExists" ]; then
   echo "DICOM ping check failed for ${host} ${aeTitle} ${port}, in directory ${fileStoragePath}" >> ${EMAILBODYFILE}
   mutt -s "DICOM ping failed ${aeTitle}" -F ${MUTTRCFILE} ${emails} < ${EMAILBODYFILE}
else
   find ${fileStoragePath} -name "*.dcm" | xargs rm -f
   fileExists=`find ${fileStoragePath} -name "*.dcm"`
   if [ ! -z "$fileExists" ]; then
      echo "DICOM ping check could not delete file ${host} ${aeTitle} ${port}, in directory ${fileStoragePath}" >> ${EMAILBODYFILE}
      mutt -s "DICOM ping delete test file failed ${aeTitle}" -F ${MUTTRCFILE} ${emails} < ${EMAILBODYFILE}
   fi
fi
rm -f ${EMAILBODYFILE}
