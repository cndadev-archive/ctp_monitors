#!/bin/bash

checks=$1
emails=$2

CURDIR=`pwd`
EMAILTXTDIR=${CURDIR}/emailTXT

emailBodyFile=${CURDIR}/.tmpEmailBodyFile.txt
touch $emailBodyFile
muttRCFile=${CURDIR}/checks.muttrc

#CURDATE=`date +%Y%m%d`

for f in ${checks}/*
do
   #echo $f
   command=`head -1 $f`
   checkName=`echo $f | sed 's/\.\///g'`
   #echo $checkName
   echo $checkName >> $emailBodyFile
   cmdOutput=`eval $command`
   #echo $cmdOutput
   echo $cmdOutput >> ${emailBodyFile}
   #echo ""
   echo ""  >> ${emailBodyFile}
   if [ ! -z "$cmdOutput" ]; then
      mutt -s "$checks" -F ${muttRCFile} ${emails} < ${emailBodyFile}
   fi
   rm ${emailBodyFile}
done
cd ..
