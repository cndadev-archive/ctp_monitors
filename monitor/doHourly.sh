#!/bin/sh

export TOOLPATH=/JavaPrograms/CTP/monitor
cd $TOOLPATH
export DCMTKDIR=
PING_AETITLE=
PING_STORAGE_DIR=
NOTIFICATION_EMAIL=


# Run small daily checks on the CTP
./dicomPingMonitor.sh ${PING_AETITLE} localhost 8103 ${PING_STORAGE_DIR} ${NOTIFICATION_EMAIL} 
./runEmptyResultChecks.sh ctp-hourly-checks ${NOTIFICATION_EMAIL} 
