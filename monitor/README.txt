# CTP Monitors 21May2015 Jenny Gurney gurneyj@wustl.edu
#
# This directory contains some very simple CTP monitors.
# Set this file to fun hourly in your cron:  doHourly.sh
# Several files need to be modified before the monitors will run correctly
#    checks.muttrc - Update with email settings 
#    ctp-hourly-checks/quarantined_sessions - Update with path to quarantine
#    doHourly.sh - Update with path to monitors and settings for DICOM ping
#    add-ping-config.xml - Update text with correct root and storage paths, then copy all text into to your config.xml. Requires CTP restart.
#
# CTP Monitor Prerequisites:
#   DCMTK, installed
#   Mutt Email Client, installed and in the path
#
# You can easily create additional emptyResult monitors by putting one-liner
#  bash scripts in the ctp-hourly-checks directory. These scripts will be run
#  hourly.  If there are no results from the script, then no email will be sent.
#  If there are any results from the script, then an email will be sent to those
#  email addresses passed to the runEmptyResultCheck.sh script.
#  (See ctp-hourly-checks/quarantined_sessions for an example.)
#
# Set up doHourly.sh in cron
# CTP Hourly Monitors
# Run monitors to check
#  1.  CTP quarantines are empty
#  2.  DICOM files are being received
42 * * * * /JavaPrograms/CTP/monitor/doHourly.sh
